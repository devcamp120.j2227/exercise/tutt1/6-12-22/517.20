import { Component } from "react";

class FormComponent extends Component {
    // {inputMessageChangeHandlerProps,outputMessageChangeHandlerProps,inputMessageProps} = this.props;
    onInputChangeHandler(event) {
        let value = event.target.value;

        this.props.inputMessageChangeHandlerProps(value);
    }

    onButtonClickHander() {
        this.props.outputMessageChangeHandlerProps();
    }
   
    render() { 
        return (
            <div>
                <div className='row mt-4'>
                    <label>Message cho bạn 12 tháng tới:</label>
                </div>
                <div className='row mt-3'>
                    <div className='col-sm-5'>
                        <input className='form-control' placeholder='điền message' onChange={(event) => this.onInputChangeHandler(event)} value={this.props.inputMessageProps}></input>
                    </div>
                </div>
                <button className='btn btn-warning mt-3' onClick={() => this.onButtonClickHander()}>Gửi thông điệp</button>
            </div>
        )
    }
}

export default FormComponent;