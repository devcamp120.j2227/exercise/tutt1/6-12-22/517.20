import { Component } from "react";
import imageLogo from "../../../src/assets/images/2.jpg"

class ResultComponent extends Component {
    render() {
        let { outputMessageProps, likeDisplayProps } = this.props;
        return (
            <div>
                <p className='text-primary'>{outputMessageProps}</p>
                <div className="mt-3">
                    {outputMessageProps.map((element, index) => {
                        return <p key={index}>{element}</p>
                    })}
                </div>
                {likeDisplayProps ? <img src={imageLogo} alt="imageLogo" style={{ width: "20%" }}></img> : null}
            </div>
        )
    }
}

export default ResultComponent;